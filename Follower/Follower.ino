#define BUZZER 3

#define MOTORAN 4
#define MOTORAP 5

#define PWMB 6
#define MOTORBN 7
#define MOTORBP 8


void setup()
{                
    pinMode(MOTORAP, OUTPUT);
    pinMode(MOTORAN, OUTPUT);
    pinMode(MOTORBP, OUTPUT);
    pinMode(MOTORBN, OUTPUT);

    digitalWrite(MOTORAP, LOW);   
    digitalWrite(MOTORAN, LOW);
    digitalWrite(MOTORBP, LOW);
    digitalWrite(MOTORBN, LOW);
  
    pinMode(PWMB, OUTPUT);
    analogWrite(PWMB, 140);
  
    pinMode(BUZZER, OUTPUT);
    digitalWrite(BUZZER, LOW);
}

void loop()
{ 
  delay(3000);
   front();
   left();
}

/*
 * Functions for moviments
*/

void back(){
       digitalWrite(MOTORBN, LOW);
       digitalWrite(MOTORBP, HIGH);
}

void front(){
       digitalWrite(MOTORBN, HIGH);
       digitalWrite(MOTORBP, LOW);
} 

void brake(){
       digitalWrite(MOTORBN, LOW);
       digitalWrite(MOTORBP, LOW);  
}

void left(){
       digitalWrite(MOTORAN, LOW);
       digitalWrite(MOTORAP, HIGH);
}

void right(){
       digitalWrite(MOTORAN, HIGH);
       digitalWrite(MOTORAP, LOW);
}   

void center(){
       digitalWrite(MOTORAN, LOW);
       digitalWrite(MOTORAP, LOW);  
}

void buzzer(){
       int frequencia;
       frequencia = 164.81;
       tone(BUZZER, frequencia);
       delay(1000);
       noTone(BUZZER);
}  

