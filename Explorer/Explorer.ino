#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Servo.h>
Servo myservo;

#define Echo 12
#define Trig 13

#define ENA 6
#define IN1 7
#define IN2 4
#define ENB 5
#define IN3 3
#define IN4 2


#define CE_PIN   9
#define CSN_PIN 10

const uint64_t pipe = 0xE8E8F0F0E1LL; // Define the transmit pipe

RF24 radio(CE_PIN, CSN_PIN); // Create a Radio

int high[1];
int low[1];

int Ultrasonic(){
  digitalWrite(Trig, LOW);
  delayMicroseconds(2);
  digitalWrite(Trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);
  unsigned long duration = pulseIn(Echo, HIGH);
  float distance = duration / 58;
  return int(distance);  
}

boolean Collision(int distance){
  Serial.println(distance);
  if(distance < 25){
    Serial.print("True");
    return true;
  }else{
    Serial.print("false");
    return false;
  }
}

char Mapping(){
  radioDistance(millis);
  myservo.write(10);
  delay(1000);
  int Right = Ultrasonic();
  myservo.write(180);
  delay(1000);
  int Left = Ultrasonic();
  myservo.write(90);
  delay(1000);
  /*
  if(direita < 10 && esquerda < 10){
    Serial.println("Back");
    return('R');
  }*/
  if(Right >= Left){
    Serial.println("Right");
    radioMovement('R');
    return('R');
  }else{
    Serial.println("Left");
    radioMovement('L');
    return('L');
  }
}

void Front(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
}
void Left(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);  
}
void Right(){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);  
}
void Back(){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);  
}
void Brake(){ //IN TEST
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
}

void radioStart(){
 int start [0];
 start [0] = 0;
 radio.write(start, sizeof(start));
 delay(200);   
}

void radioFinish(){
 int finish[0];
 finish[0] = 1;
 radio.write(finish, sizeof(finish));
 delay(200);   
}

void radioMovement(char movement){
 char movem [0];
 movem[0] = movement;
 radio.write(movem, sizeof(movem));
 delay(200);   
}

void radioDistance(unsigned long distance){
 int distanc [0];
 distanc[0] = distance;
 radio.write(distanc, sizeof(distanc));
 delay(200);   
}

void setup(){
//Radio
 radio.begin();
 radio.openWritingPipe(pipe);
//Servo
  myservo.attach(8);
  myservo.write(90);//10-90-180
//Ultrasonic  
  Serial.begin(9600);//Serial Port
  pinMode(Trig, OUTPUT);
  pinMode(Echo, INPUT);
//Motors  
  pinMode(ENA, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
//PWM
  analogWrite(ENA, 80);//Right
  analogWrite(ENB, 90);//Left + 10
//Delay 
  delay(5000);
  Front();
}

void loop(){    
    if(Collision(Ultrasonic()) == true){
      Brake();
      if(Mapping() == 'R'){
        Right();
        delay(450);
        millis = 0;
      }else{
        Left();
        delay(450);
        millis = 0LL;
        myservo.write(10);
        delay(20);
        while(Ultrasonic() < 30){
          Front();
        }
      }     
    }else{
      Front();
    }
    if(sensor < 250){
      Break();
      radioFinish();
    }
}
